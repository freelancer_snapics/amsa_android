package com.snapics.amsa.Helper.httpHelper;

import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.snapics.amsa.Constants.ApiConstants;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/*
 * Created by mac on 7/27/17.
 */
public class HttpHelper {

    private static HttpHelper instance;
    private Retrofit retrofit;
    private ApiMethods apiMethods;

    public synchronized static HttpHelper getInstance() {
        if (instance == null) {
            instance = new HttpHelper();
        }
        return instance;
    }

    private HttpHelper() {
        retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .client(getClient())
                .baseUrl(ApiConstants.BASE_URL)
                .build();
    }

    private OkHttpClient getClient() {
        return new OkHttpClient.Builder()
                .addNetworkInterceptor(new StethoInterceptor())
                .build();
    }

    public ApiMethods getApiMethods() {
        if (apiMethods == null) {
            apiMethods = retrofit.create(ApiMethods.class);
        }
        return apiMethods;
    }
}