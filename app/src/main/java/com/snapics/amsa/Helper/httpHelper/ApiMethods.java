package com.snapics.amsa.Helper.httpHelper;

import com.snapics.amsa.Constants.ApiConstants;
import com.snapics.amsa.Model.Categoriesmodel;
import com.snapics.amsa.Model.RequestCategoryWrapper;
import com.snapics.amsa.Model.SyncRequestCategoryWrapper;
import com.snapics.amsa.Model.UserModel;

import java.util.List;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by mac on 7/27/17.
 */

public interface ApiMethods {

    @POST(ApiConstants.LOGIN_API)
    Call<UserModel> login(@Query("username") String username, @Query("password") String password);

    @GET(ApiConstants.CATEGORIES_API)
    Call<List<Categoriesmodel>> getCategories(@Path("id") String id);

    @POST(ApiConstants.SAVE_CAEGORIES_API)
    Call<ResponseBody> saveData(@Body RequestCategoryWrapper body);

    @POST(ApiConstants.SAVE_CAEGORIES_API)
    Call<ResponseBody> saveData(@Body SyncRequestCategoryWrapper body);

    @Multipart
    @POST(ApiConstants.UPLOAD_IMAGE)
    Call<ResponseBody> uploadImage(@Part("file\"; filename=\"pp.png\"") RequestBody image);

}
