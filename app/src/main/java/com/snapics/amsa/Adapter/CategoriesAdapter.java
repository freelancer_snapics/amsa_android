package com.snapics.amsa.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.snapics.amsa.Model.FilteredModel;
import com.snapics.amsa.R;

import java.io.File;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Mohamed Hamdan on 2017-Oct-03.
 * mohamed.nayef95@gmail.com
 */
public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.ViewHolder> {

    private Context context;
    private List<FilteredModel> categories;
    private OnClickListener listener;

    public CategoriesAdapter(Context context, List<FilteredModel> categories) {
        this.context = context;
        this.categories = categories;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_category, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(categories.get(position));
    }

    @Override
    public int getItemCount() {
        return categories.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvTitle)
        AppCompatTextView tvTitle;
        @BindView(R.id.rvSubCategories)
        RecyclerView rvSubCategories;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(final FilteredModel model) {
            if (getAdapterPosition() != RecyclerView.NO_POSITION) {
                rvSubCategories.setNestedScrollingEnabled(false);
                tvTitle.setText(model.getTitle());
                rvSubCategories.setLayoutManager(new LinearLayoutManager(context));
                rvSubCategories.setAdapter(new SubCategoriesAdapter(context, model.getCategoriesmodelList(), getAdapterPosition(), listener));
                rvSubCategories.setVisibility(model.isSelected() ? View.VISIBLE : View.GONE);
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        model.setSelected(!model.isSelected());
                        rvSubCategories.setVisibility(model.isSelected() ? View.VISIBLE : View.GONE);
                    }
                });
            }
        }
    }

    public void setOnClickListener(OnClickListener listener) {
        this.listener = listener;
    }

    public void onActivityResult(int flatPosition, int childIndex, File file) {
        categories.get(flatPosition).getCategoriesmodelList().get(childIndex).setButtonColor(Color.GREEN);
        categories.get(flatPosition).getCategoriesmodelList().get(childIndex).setImageFile(file);
        notifyDataSetChanged();
    }

    public interface OnClickListener {
        void onItemClick(int flatPosition, int childIndex);
    }
}
