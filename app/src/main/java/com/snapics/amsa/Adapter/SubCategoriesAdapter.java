package com.snapics.amsa.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import com.snapics.amsa.Model.Categoriesmodel;
import com.snapics.amsa.R;
import com.snapics.amsa.Utilities.Utilities;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Mohamed Hamdan on 2017-Oct-03.
 * mohamed.nayef95@gmail.com
 */
class SubCategoriesAdapter extends RecyclerView.Adapter<SubCategoriesAdapter.ViewHolder> {

    private Context context;
    private List<Categoriesmodel> categories;
    private int parentPosition = 0;
    private CategoriesAdapter.OnClickListener listener;

    public SubCategoriesAdapter(Context context, List<Categoriesmodel> categories, int parentPosition, CategoriesAdapter.OnClickListener listener) {
        this.context = context;
        this.categories = categories;
        this.parentPosition = parentPosition;
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_sub_category, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(categories.get(position));
    }

    @Override
    public int getItemCount() {
        return categories.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_item)
        TextView tvItemName;
        @BindView(R.id.tv_details)
        TextView tvDetails;
        @BindView(R.id.order_table_row_present_CheckBox)
        CheckBox cpOrder;
        @BindView(R.id.et_count)
        EditText etCount;
        @BindView(R.id.et_note)
        EditText etNote;
        @BindView(R.id.subButton)
        public Button subButton;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(final Categoriesmodel categoriesmodel) {
            tvItemName.setText(categoriesmodel.getITM_NAME());
            //Freeze Qty=0 Diff =0 Balance= 10090
            tvDetails.setText("Freeze Qty= " + categoriesmodel.getFREEZE_QTY() + " Diff= " + 0 + " Balance= " + categoriesmodel.getADS_BAL());
            tvDetails.setTextColor(Utilities.getColor(categoriesmodel.getCOLOR()));

            etCount.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View view, boolean b) {
                    if (!etCount.getText().toString().isEmpty())
                        categoriesmodel.settAKINGQTY(Integer.parseInt(etCount.getText().toString()));
                }
            });
            etCount.setText(String.valueOf(categoriesmodel.gettAKINGQTY() != -1 ? categoriesmodel.gettAKINGQTY() : ""));
            cpOrder.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    categoriesmodel.setuRGENTFLAG(b ? 1 : 0);
                }
            });
            cpOrder.setChecked(categoriesmodel.getuRGENTFLAG() == 1);

            if (categoriesmodel.getButtonColor() != -1) {
                subButton.setBackgroundColor(categoriesmodel.getButtonColor());
            }
            subButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (getAdapterPosition() != RecyclerView.NO_POSITION) {
                        listener.onItemClick(parentPosition, getAdapterPosition());
                    }
                }
            });
        }
    }
}
