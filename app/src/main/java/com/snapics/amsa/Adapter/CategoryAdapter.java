package com.snapics.amsa.Adapter;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.snapics.amsa.Holder.CategoryViewHolder;
import com.snapics.amsa.Holder.SubCategoryViewHolder;
import com.snapics.amsa.Model.Categoriesmodel;
import com.snapics.amsa.Model.FilteredModel;
import com.snapics.amsa.R;
import com.thoughtbot.expandablecheckrecyclerview.CheckableChildRecyclerViewAdapter;
import com.thoughtbot.expandablecheckrecyclerview.models.CheckedExpandableGroup;
import com.thoughtbot.expandablecheckrecyclerview.models.MultiCheckExpandableGroup;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.io.File;
import java.util.List;

/**
 * Created by mac on 7/28/17.
 */

public class CategoryAdapter extends CheckableChildRecyclerViewAdapter<CategoryViewHolder, SubCategoryViewHolder> {

    private List<? extends MultiCheckExpandableGroup> groups;
    private OnClickListener listener;

    public CategoryAdapter(List<? extends MultiCheckExpandableGroup> groups) {
        super(groups);
        this.groups = groups;
    }

    @Override
    public CategoryViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_category, parent, false);
        return new CategoryViewHolder(view);
    }

    @Override
    public SubCategoryViewHolder onCreateCheckChildViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_sub_category, parent, false);
        return new SubCategoryViewHolder(view);
    }

    @Override
    public void onBindCheckChildViewHolder(SubCategoryViewHolder holder, final int flatPosition,
                                           CheckedExpandableGroup group, final int childIndex) {
        final Categoriesmodel categoriesmodel = ((FilteredModel) group).getCategoriesmodelList().get(childIndex);
        holder.onBind(categoriesmodel, flatPosition, childIndex, this);
        if (categoriesmodel.getButtonColor() != -1) {
            holder.subButton.setBackgroundColor(categoriesmodel.getButtonColor());
        }
        holder.subButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onItemClick(flatPosition, childIndex);
            }
        });
    }

    @Override
    public void onBindGroupViewHolder(CategoryViewHolder holder, int flatPosition, ExpandableGroup group) {
        final String title = (group).getTitle();
        holder.onBind(title);
    }

    public void setOnClickListener(OnClickListener listener) {
        this.listener = listener;
    }

    public void onActivityResult(int flatPosition, int childIndex, File file) {
        ((FilteredModel) groups.get(flatPosition)).getCategoriesmodelList().get(childIndex).setButtonColor(Color.GREEN);
        ((FilteredModel) groups.get(flatPosition)).getCategoriesmodelList().get(childIndex).setImageFile(file);
        notifyDataSetChanged();
    }

    public interface OnClickListener {
        void onItemClick(int flatPosition, int childIndex);
    }
}
