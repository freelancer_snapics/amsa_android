package com.snapics.amsa.Holder;

import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Checkable;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import com.snapics.amsa.Model.Categoriesmodel;
import com.snapics.amsa.R;
import com.snapics.amsa.Utilities.Utilities;
import com.thoughtbot.expandablecheckrecyclerview.CheckableChildRecyclerViewAdapter;
import com.thoughtbot.expandablecheckrecyclerview.viewholders.CheckableChildViewHolder;

import butterknife.BindView;
import butterknife.ButterKnife;

/*
 * Created by mac on 7/28/17.
 */
public class SubCategoryViewHolder extends CheckableChildViewHolder {

    @BindView(R.id.tv_item)
    TextView tvItemName;
    @BindView(R.id.tv_details)
    TextView tvDetails;
    @BindView(R.id.order_table_row_present_CheckBox)
    CheckBox cpOrder;
    @BindView(R.id.et_count)
    EditText etCount;
    @BindView(R.id.et_note)
    EditText etNote;
    @BindView(R.id.subButton)
    public Button subButton;

    public SubCategoryViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);

    }

    @Override
    public Checkable getCheckable() {
        return cpOrder;
    }

    public void onBind(final Categoriesmodel categoriesmodel, final int groupIndex, final int childIndex, final CheckableChildRecyclerViewAdapter adapter) {
        tvItemName.setText(categoriesmodel.getITM_NAME());
        //Freeze Qty=0 Diff =0 Balance= 10090
        tvDetails.setText("Freeze Qty= " + categoriesmodel.getFREEZE_QTY() + " Diff= " + 0 + " Balance= " + categoriesmodel.getADS_BAL());
        tvDetails.setTextColor(Utilities.getColor(categoriesmodel.getCOLOR()));

        etCount.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (!etCount.getText().toString().isEmpty())
                    categoriesmodel.settAKINGQTY(Integer.parseInt(etCount.getText().toString()));
            }
        });
        etCount.setText(String.valueOf(categoriesmodel.gettAKINGQTY() != -1 ? categoriesmodel.gettAKINGQTY() : ""));
        cpOrder.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                categoriesmodel.setuRGENTFLAG(b ? 1 : 0);
            }
        });

//        ResultCallback<DriveApi.DriveContentsResult> contentsCallback = new
//                ResultCallback<DriveApi.DriveContentsResult>() {
//                    @Override
//                    public void onResult(@NonNull DriveApi.DriveContentsResult result) {
//                        if (!result.getStatus().isSuccess()) {
//                            return;
//                        }
//
//                        MetadataChangeSet changeSet = new MetadataChangeSet.Builder()
//                                .setTitle("New file")
//                                .setMimeType("image/*").build();
//                        Drive.DriveApi.getRootFolder(getGoogleApiClient())
//                                .createFile(getGoogleApiClient(), changeSet, result.getDriveContents())
//                                .setResultCallback(this);
//                    }
//                };
    }
}
