package com.snapics.amsa.Model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import io.realm.RealmList
import io.realm.RealmModel
import io.realm.annotations.RealmClass

/**
 * Created by Mohamed Hamdan on 2017-Aug-02.
 * mohamed.nayef95@gmail.com
 */
@RealmClass
open class RequestCategoryWrapper : RealmModel {

    @SerializedName("PHARMACY_ACC")
    @Expose
    var pharmacyacc: String? = null
    @SerializedName("USER")
    @Expose
    var user: String? = null
    @SerializedName("NOTES")
    @Expose
    var notes: String? = null
    @SerializedName("PRE_POHOTO")
    @Expose
    var prepohoto: String? = null
    @SerializedName("POST_POHOTO")
    @Expose
    var postpohoto: String? = null
    @SerializedName("pharmaTakingDetails")
    @Expose
    var pharmaTakingDetails: RealmList<PharmaTakingDetail>? = null

    var isDeleted: Boolean = false

    override fun toString(): String {
        return "RequestCategoryWrapper(pharmacyacc=$pharmacyacc, user=$user, notes=$notes, prepohoto=$prepohoto, postpohoto=$postpohoto)"
    }


}