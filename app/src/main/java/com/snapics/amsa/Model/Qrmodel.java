package com.snapics.amsa.Model;

import io.realm.RealmModel;
import io.realm.annotations.RealmClass;
import io.realm.annotations.Required;

@RealmClass
public class Qrmodel implements RealmModel {

    @Required
    private String QR_ID;
    @Required
    private String NAME;

    public String getQr_Id() {
        return QR_ID;
    }

    public void setQr_Id(String QR_ID) {
        this.QR_ID = QR_ID;
    }

    public String getName() {
        return NAME;
    }

    public void setName(String NAME) {
        this.NAME = NAME;
    }

}
