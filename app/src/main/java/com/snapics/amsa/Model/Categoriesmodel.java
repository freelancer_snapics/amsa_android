package com.snapics.amsa.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.io.File;

import io.realm.RealmModel;
import io.realm.annotations.Ignore;
import io.realm.annotations.RealmClass;

/*
 * Created by mac on 7/28/17.
 */

@RealmClass
public class Categoriesmodel implements RealmModel, Parcelable {

    //
    @SerializedName("COLOR")
    private String COLOR;

    @Ignore
    @SerializedName("ErrorMessage")
    private String errorMessage;

    @Ignore
    @SerializedName("FREEZE_DATE")
    private String fREEZEDATE;

    //
    @SerializedName("ITM_NAME")
    private String ITM_NAME;

    //
    @SerializedName("ITM_SUB_CAT")
    private String ITM_SUB_CAT;

    //
    @SerializedName("PHARAMCY_ACC")
    private String PHARAMCY_ACC;

    //
    @SerializedName("PHARMACY_NAME")
    private String PHARMACY_NAME;

    @Ignore
    @SerializedName("PHARMA_CHAIN")
    private int pHARMACHAIN;

    //
    @SerializedName("ITM_CAT")
    private String ITM_CAT;

    //
    @SerializedName("ITM_PART")
    private String ITM_PART;

    //
    @SerializedName("FREEZE_QTY")
    private String FREEZE_QTY;

    @Ignore
    @SerializedName("TAKING_QTY")
    private int tAKINGQTY = -1;

    @Ignore
    @SerializedName("NOTES")
    private String nOTES;

    @Ignore
    private File imageFile;

    @Ignore
    private int buttonColor = -1;

    @Ignore
    @SerializedName("URGENT_FLAG")
    private int uRGENTFLAG;

    //
    @SerializedName("ADS_BAL")
    private String ADS_BAL;

    @Ignore
    @SerializedName("PTID_PHOTO")
    private String pTIDPHOTO;

    public String getCOLOR() {
        return COLOR;
    }

    public void setCOLOR(String COLOR) {
        this.COLOR = COLOR;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getfREEZEDATE() {
        return fREEZEDATE;
    }

    public void setfREEZEDATE(String fREEZEDATE) {
        this.fREEZEDATE = fREEZEDATE;
    }

    public String getITM_NAME() {
        return ITM_NAME;
    }

    public void setITM_NAME(String ITM_NAME) {
        this.ITM_NAME = ITM_NAME;
    }

    public String getITM_SUB_CAT() {
        return ITM_SUB_CAT;
    }

    public void setITM_SUB_CAT(String ITM_SUB_CAT) {
        this.ITM_SUB_CAT = ITM_SUB_CAT;
    }

    public String getPHARAMCY_ACC() {
        return PHARAMCY_ACC;
    }

    public void setPHARAMCY_ACC(String PHARAMCY_ACC) {
        this.PHARAMCY_ACC = PHARAMCY_ACC;
    }

    public String getPHARMACY_NAME() {
        return PHARMACY_NAME;
    }

    public void setPHARMACY_NAME(String PHARMACY_NAME) {
        this.PHARMACY_NAME = PHARMACY_NAME;
    }

    public int getpHARMACHAIN() {
        return pHARMACHAIN;
    }

    public void setpHARMACHAIN(int pHARMACHAIN) {
        this.pHARMACHAIN = pHARMACHAIN;
    }

    public String getITM_CAT() {
        return ITM_CAT;
    }

    public void setITM_CAT(String ITM_CAT) {
        this.ITM_CAT = ITM_CAT;
    }

    public String getITM_PART() {
        return ITM_PART;
    }

    public void setITM_PART(String ITM_PART) {
        this.ITM_PART = ITM_PART;
    }

    public String getFREEZE_QTY() {
        return FREEZE_QTY;
    }

    public void setFREEZE_QTY(String FREEZE_QTY) {
        this.FREEZE_QTY = FREEZE_QTY;
    }

    public int gettAKINGQTY() {
        return tAKINGQTY;
    }

    public void settAKINGQTY(int tAKINGQTY) {
        this.tAKINGQTY = tAKINGQTY;
    }

    public String getnOTES() {
        return nOTES;
    }

    public void setnOTES(String nOTES) {
        this.nOTES = nOTES;
    }

    public int getuRGENTFLAG() {
        return uRGENTFLAG;
    }

    public void setuRGENTFLAG(int uRGENTFLAG) {
        this.uRGENTFLAG = uRGENTFLAG;
    }

    public String getADS_BAL() {
        return ADS_BAL;
    }

    public void setADS_BAL(String ADS_BAL) {
        this.ADS_BAL = ADS_BAL;
    }

    public String getpTIDPHOTO() {
        return pTIDPHOTO;
    }

    public void setpTIDPHOTO(String pTIDPHOTO) {
        this.pTIDPHOTO = pTIDPHOTO;
    }

    public static Creator<Categoriesmodel> getCREATOR() {
        return CREATOR;
    }

    public Categoriesmodel() {
    }

    public Categoriesmodel(Parcel in) {
        ADS_BAL = in.readString();
        COLOR = in.readString();
        errorMessage = in.readString();
        fREEZEDATE = in.readString();
        FREEZE_QTY = in.readString();
        ITM_CAT = in.readString();
        ITM_NAME = in.readString();
        ITM_PART = in.readString();
        ITM_SUB_CAT = in.readString();
        PHARAMCY_ACC = in.readString();
        PHARMACY_NAME = in.readString();
        pHARMACHAIN = in.readInt();
    }


    public static final Creator<Categoriesmodel> CREATOR = new Creator<Categoriesmodel>() {
        @Override
        public Categoriesmodel createFromParcel(Parcel in) {
            return new Categoriesmodel(in);
        }

        @Override
        public Categoriesmodel[] newArray(int size) {
            return new Categoriesmodel[size];
        }
    };

    public void setImageFile(File imageFile) {
        this.imageFile = imageFile;
    }

    public File getImageFile() {
        return imageFile;
    }

    public int getButtonColor() {
        return buttonColor;
    }

    public void setButtonColor(int buttonColor) {
        this.buttonColor = buttonColor;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(ADS_BAL);
        parcel.writeString(COLOR);
        parcel.writeString(errorMessage);
        parcel.writeString(fREEZEDATE);
        parcel.writeString(FREEZE_QTY);
        parcel.writeString(ITM_CAT);
        parcel.writeString(ITM_NAME);
        parcel.writeString(ITM_PART);
        parcel.writeString(ITM_SUB_CAT);
        parcel.writeString(PHARAMCY_ACC);
        parcel.writeString(PHARMACY_NAME);
        parcel.writeInt(pHARMACHAIN);
    }
}
