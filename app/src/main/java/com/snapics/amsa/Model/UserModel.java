package com.snapics.amsa.Model;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmModel;
import io.realm.annotations.RealmClass;

/**
 * Created by mac on 7/27/17.
 */

@RealmClass
public class UserModel implements RealmModel {
    @SerializedName("DateTimeNow")
    private String dateTimeNow;
    @SerializedName("ErrorMessage")
    private String errorMessage;
    @SerializedName("FIRSTNAME")
    private String fIRSTNAME;
    @SerializedName("LASTNAME")
    private String lASTNAME;
    @SerializedName("PASSWORD")
    private String pASSWORD;
    @SerializedName("USERNAME")
    private String uSERNAME;
    @SerializedName("USER_TYPE")
    private String uSERTYPE;

    public void setDateTimeNow(String dateTimeNow) {
        this.dateTimeNow = dateTimeNow;
    }

    public String getDateTimeNow() {
        return this.dateTimeNow;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorMessage() {
        return this.errorMessage;
    }

    public void setFIRSTNAME(String fIRSTNAME) {
        this.fIRSTNAME = fIRSTNAME;
    }

    public String getFIRSTNAME() {
        return this.fIRSTNAME;
    }

    public void setLASTNAME(String lASTNAME) {
        this.lASTNAME = lASTNAME;
    }

    public String getLASTNAME() {
        return this.lASTNAME;
    }

    public void setPASSWORD(String pASSWORD) {
        this.pASSWORD = pASSWORD;
    }

    public Object getPASSWORD() {
        return this.pASSWORD;
    }

    public void setUSERNAME(String uSERNAME) {
        this.uSERNAME = uSERNAME;
    }

    public String getUSERNAME() {
        return this.uSERNAME;
    }

    public void setUSERTYPE(String uSERTYPE) {
        this.uSERTYPE = uSERTYPE;
    }

    public String getUSERTYPE() {
        return this.uSERTYPE;
    }
}
