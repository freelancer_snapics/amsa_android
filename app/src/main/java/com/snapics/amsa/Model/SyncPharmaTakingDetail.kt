package com.snapics.amsa.Model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import io.realm.RealmModel

/**
 * Created by Mohamed Hamdan on 2017-Aug-02.
 * mohamed.nayef95@gmail.com
 */
open class SyncPharmaTakingDetail {

    @SerializedName("ITM_CAT")
    @Expose
    var itmcat: String? = null
    @SerializedName("ITM_PART")
    @Expose
    var itmpart: String? = null
    @SerializedName("ITM_NAME")
    @Expose
    var itmname: String? = null
    @SerializedName("FREEZE_QTY")
    @Expose
    var freezeqty: String? = null
    @SerializedName("TAKING_QTY")
    @Expose
    var takingqty: String? = null
    @SerializedName("NOTES")
    @Expose
    var notes: String? = null
    @SerializedName("URGENT_FLAG")
    @Expose
    var urgentflag: Int = 0
    @SerializedName("ADS_BAL")
    @Expose
    var adsbal: String? = null
    @SerializedName("PTID_PHOTO")
    @Expose
    var ptidphoto: String? = null

    override fun toString(): String {
        return "PharmaTakingDetail(itmcat=$itmcat, itmpart=$itmpart, itmname=$itmname, freezeqty=$freezeqty, takingqty=$takingqty, notes=$notes, urgentflag=$urgentflag, adsbal=$adsbal, ptidphoto=$ptidphoto)"
    }


}