package com.snapics.amsa.Model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by Mohamed Hamdan on 2017-Aug-02.
 * mohamed.nayef95@gmail.com
 */
open class SyncRequestCategoryWrapper {

    @SerializedName("PHARMACY_ACC")
    @Expose
    var pharmacyacc: String? = null
    @SerializedName("USER")
    @Expose
    var user: String? = null
    @SerializedName("NOTES")
    @Expose
    var notes: String? = null
    @SerializedName("PRE_POHOTO")
    @Expose
    var prepohoto: String? = null
    @SerializedName("POST_POHOTO")
    @Expose
    var postpohoto: String? = null
    @SerializedName("pharmaTakingDetails")
    @Expose
    var pharmaTakingDetails: MutableList<SyncPharmaTakingDetail>? = null

    override fun toString(): String {
        return "RequestCategoryWrapper(pharmacyacc=$pharmacyacc, user=$user, notes=$notes, prepohoto=$prepohoto, postpohoto=$postpohoto)"
    }


}