package com.snapics.amsa.Model;

import com.thoughtbot.expandablecheckrecyclerview.models.MultiCheckExpandableGroup;

import java.util.List;

/**
 * Created by mac on 7/28/17.
 */

public class FilteredModel extends MultiCheckExpandableGroup {

    private String title;

    private List<Categoriesmodel> categoriesmodelList;
    private boolean selected;

    public FilteredModel(String title, List<Categoriesmodel> categoriesmodelList) {
        super(title, categoriesmodelList);

        this.title = title;
        this.categoriesmodelList = categoriesmodelList;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Categoriesmodel> getCategoriesmodelList() {
        return categoriesmodelList;
    }

    public void setCategoriesmodelList(List<Categoriesmodel> categoriesmodelList) {
        this.categoriesmodelList = categoriesmodelList;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
