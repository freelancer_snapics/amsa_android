package com.snapics.amsa.Utilities;

import android.content.Context;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;

import java.util.List;

/**
 * Created by mac on 7/28/17.
 */

public class Utilities {

    public static boolean isAndroidEmulator() {
        String model = Build.MODEL;
        String product = Build.PRODUCT;
        boolean isEmulator = false;
        if (product != null) {
            isEmulator = product.equals("sdk") || product.contains("_sdk") || product.contains("sdk_");
        }
        return isEmulator;
    }

    public static boolean isWifi(Context cxt) {
        ConnectivityManager cm = (ConnectivityManager) cxt.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm != null && cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().getType() == ConnectivityManager.TYPE_WIFI;
    }

    public static boolean isNetworkAvailable(Context context) {
        if (context == null) {
            return false;
        }
        if (isWifi(context)) {
            return true;
        }
        try {
            ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (connectivity != null) {
                NetworkInfo info = connectivity.getActiveNetworkInfo();
                return info.isAvailable();
            }
        } catch (Exception e) {
            return false;
        }
        return false;
    }

    public static <T extends List<?>> T cast(Object obj) {
        return (T) obj;
    }

    public static int getColor(String colorName) {
        switch (colorName.toLowerCase()) {
            case "green":
                return Color.parseColor("#4CAF50");
            case "orange":
                return Color.parseColor("#FF9800");
            case "red":
                return Color.parseColor("#F44336");
            case "blue":
                return Color.parseColor("#2196F3");
            case "purple":
                return Color.parseColor("#9C27B0");
            case "gray":
                return Color.parseColor("#9E9E9E");
            case "pink":
                return Color.parseColor("#E91E63");
            default:
                return Color.BLACK;
        }
    }
}
