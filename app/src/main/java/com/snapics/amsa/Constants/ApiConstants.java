package com.snapics.amsa.Constants;

/**
 * Created by mac on 7/27/17.
 */

public class ApiConstants {

    public static final String BASE_URL = "https://www.amsa-merchandiser.com";
    public static final String LOGIN_API = "/Merchandisor/api/Login/Login";
    public static final String CATEGORIES_API = "/Merchandisor/api/Categories/GetCategories/{id}";
    public static final String SAVE_CAEGORIES_API = "/Merchandisor/api/StockTaking/SavePharmaTakingMaster";
    public static final String UPLOAD_IMAGE = "/StockTakingService/api/upload";

    public static final String IMAGES_PATH = "https://amsa-merchandiser.com/stocktakingservice/Uploads/";
}
