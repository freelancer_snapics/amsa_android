package com.snapics.amsa.data

import android.support.annotation.UiThread
import com.snapics.amsa.Model.Categoriesmodel
import com.snapics.amsa.Model.PharmaTakingDetail
import com.snapics.amsa.Model.Qrmodel
import com.snapics.amsa.Model.RequestCategoryWrapper
import com.snapics.amsa.Model.UserModel
import io.realm.Case.INSENSITIVE
import io.realm.Realm
import io.realm.RealmConfiguration
import io.realm.RealmModel
import io.realm.RealmResults
import io.realm.Sort
import io.realm.Sort.DESCENDING
import io.realm.annotations.RealmModule

/**
 * Created by Mohamed Hamdan on 2017-Jul-13.
 * mohamed.nayef95@gmail.com
 */

@RealmModule(classes = arrayOf(Qrmodel::class, Categoriesmodel::class))
class LocalDataBase

@RealmModule(classes = arrayOf(UserModel::class, PharmaTakingDetail::class, RequestCategoryWrapper::class))
class RemoteDataBase

class DbHelper {

    private var realm: Realm

    @UiThread
    companion object {
        val instance = DbHelper()

        val instance1 = DbHelper("default1.realm")
    }

    constructor() {

        val configuration = RealmConfiguration.Builder()
                .assetFile("database/default.realm")
                .migration { realm, _, _ ->
                    realm.schema.get("Categoriesmodel").setNullable("COLOR", true)
                            .setNullable("ITM_NAME", true)
                            .setNullable("ITM_SUB_CAT", true)
                            .setNullable("PHARAMCY_ACC", true)
                            .setNullable("PHARMACY_NAME", true)
                            .setNullable("ITM_CAT", true)
                            .setNullable("ITM_PART", true)
                            .setNullable("FREEZE_QTY", true)
                            .setNullable("ADS_BAL", true)
                }
                .modules(LocalDataBase())
                .build()
        realm = Realm.getInstance(configuration)
    }

    constructor(str: String) {
        val configuration = RealmConfiguration.Builder()
                .name(str)
                .modules(RemoteDataBase())
                .build()
        realm = Realm.getInstance(configuration)
    }


    @UiThread
    fun <T : RealmModel> insert(clazz: T) {
        realm.beginTransaction()
        realm.copyToRealm(clazz)
        realm.commitTransaction()
    }

    @UiThread
    fun <T : RealmModel> insert(list: List<T>) {
        realm.beginTransaction()
        realm.copyToRealm(list)
        realm.commitTransaction()
    }

    @UiThread
    fun <T : RealmModel> getAll(clazz: Class<T>): RealmResults<T>? {
        return realm.where(clazz).findAll()
    }

    @UiThread
    fun <T : RealmModel> getFirst(clazz: Class<T>): T? {
        return realm.where(clazz).findFirst()
    }

    @UiThread
    fun <T : RealmModel> getAllByColVal(clazz: Class<T>, col: String, `val`: String): RealmResults<T>? {
        return realm.where(clazz).equalTo(col, `val`).findAll()
    }


    @UiThread
    fun <T : RealmModel> getAllByColVal(clazz: Class<T>, col: String, `val`: String, sortName: String, sort: Sort): RealmResults<T>? {
        return realm.where(clazz).equalTo(col, `val`).findAllSorted(sortName, sort)
    }

    @UiThread
    fun <T : RealmModel> getAllByColsVals(clazz: Class<T>, cols: Array<String>, vals: Array<String>): RealmResults<T>? {
        val query = realm.where(clazz)
        for (i in 0..vals.size - 1) {
            query.equalTo(cols[i], vals[i], INSENSITIVE)
        }
        query.or()
        for ((j, i) in ((vals.size - 1) downTo 0).withIndex()) {
            query.equalTo(cols[j], vals[i], INSENSITIVE)
        }
        return query.findAll()
    }

    @UiThread
    fun <T : RealmModel> getAllByColsVals(clazz: Class<T>, cols: Array<String>, vals: Array<String>, sortBy: String): RealmResults<T>? {
        val query = realm.where(clazz)
        for (i in 0..vals.size - 1) {
            query.equalTo(cols[i], vals[i], INSENSITIVE)
        }
        query.or()
        for ((j, i) in ((vals.size - 1) downTo 0).withIndex()) {
            query.equalTo(cols[j], vals[i], INSENSITIVE)
        }
        return query.findAllSorted(sortBy, DESCENDING)
    }

    @UiThread
    fun <T : RealmModel> getFirstByColsVals(clazz: Class<T>, cols: Array<String>, vals: Array<String>): T? {
        val query = realm.where(clazz)
        for (i in 0..vals.size - 1) {
            query.equalTo(cols[i], vals[i], INSENSITIVE)
        }
        query.or()
        for ((j, i) in ((vals.size - 1) downTo 0).withIndex()) {
            query.equalTo(cols[j], vals[i], INSENSITIVE)
        }
        return query.findFirst()
    }

    @UiThread
    fun <T : RealmModel> getAllByColVal(clazz: Class<T>, col: String, `val`: Boolean): RealmResults<T>? {
        return realm.where(clazz).equalTo(col, `val`).findAll()
    }

    @UiThread
    fun <T : RealmModel> getAllByColsVals(clazz: Class<T>, cols: Array<String>, vals: Array<Boolean>): RealmResults<T>? {
        val query = realm.where(clazz)
        for (i in 0..vals.size) {
            query.equalTo(cols[i], vals[i])
        }
        return query.findAll()
    }

    @UiThread
    fun <T : RealmModel> getAllByColVal(clazz: Class<T>, col: String, `val`: Double): RealmResults<T>? {
        return realm.where(clazz).equalTo(col, `val`).findAll()
    }

    @UiThread
    fun <T : RealmModel> getAllByColsVals(clazz: Class<T>, cols: Array<String>, vals: Array<Double>): RealmResults<T>? {
        val query = realm.where(clazz)
        for (i in 0..vals.size) {
            query.equalTo(cols[i], vals[i])
        }
        return query.findAll()
    }

    @UiThread
    fun <T : RealmModel> getAllByColVal(clazz: Class<T>, col: String, `val`: Int): RealmResults<T>? {
        return realm.where(clazz).equalTo(col, `val`).findAll()
    }

    @UiThread
    fun <T : RealmModel> getAllByColsVals(clazz: Class<T>, cols: Array<String>, vals: Array<Int>): RealmResults<T>? {
        val query = realm.where(clazz)
        for (i in 0..vals.size) {
            query.equalTo(cols[i], vals[i])
        }
        return query.findAll()
    }

    @UiThread
    fun <T : RealmModel> getFirstByColVal(clazz: Class<T>, col: String, `val`: String): T? {
        return realm.where(clazz).equalTo(col, `val`).findFirst()
    }

    @UiThread
    fun <T : RealmModel> getFirstByColVal(clazz: Class<T>, col: String, `val`: Boolean): T? {
        return realm.where(clazz).equalTo(col, `val`).findFirst()
    }

    @UiThread
    fun <T : RealmModel> getFirstByColVal(clazz: Class<T>, col: String, `val`: Double): T? {
        return realm.where(clazz).equalTo(col, `val`).findFirst()
    }

    @UiThread
    fun <T : RealmModel> getFirstByColVal(clazz: Class<T>, col: String, `val`: Int): T? {
        return realm.where(clazz).equalTo(col, `val`).findFirst()
    }

    @UiThread
    fun <T : RealmModel> deleteAll(clazz: Class<T>) {
        realm.beginTransaction()
        realm.delete(clazz)
        realm.commitTransaction()
    }

    @UiThread
    fun <T : RealmModel> deleteByColVal(clazz: Class<T>, col: String, `val`: String) {
        realm.beginTransaction()
        realm.where(clazz).equalTo(col, `val`).findAll().deleteFirstFromRealm()
        realm.commitTransaction()
    }

    @UiThread
    fun <T : RealmModel> deleteByColVal(clazz: Class<T>, col: String, `val`: Double) {
        realm.beginTransaction()
        realm.where(clazz).equalTo(col, `val`).findAll().deleteFirstFromRealm()
        realm.commitTransaction()
    }

    @UiThread
    fun <T : RealmModel> deleteByColVal(clazz: Class<T>, col: String, `val`: Int) {
        realm.beginTransaction()
        realm.where(clazz).equalTo(col, `val`).findAll().deleteFirstFromRealm()
        realm.commitTransaction()
    }

    @UiThread
    fun <T : RealmModel> deleteByColVal(clazz: Class<T>, col: String, `val`: Boolean) {
        realm.beginTransaction()
        realm.where(clazz).equalTo(col, `val`).findAll().deleteFirstFromRealm()
        realm.commitTransaction()
    }

    @UiThread
    fun <T : RealmModel> getCount(clazz: Class<T>): Long {
        return realm.where(clazz).count()
    }

    fun beginTransaction() {
        realm.beginTransaction()
    }

    fun commitTransaction() {
        realm.commitTransaction()
    }

    fun <T : RealmModel> getAllContainsVal(clazz: Class<T>, col: String, `val`: String): RealmResults<T>? {
        return realm.where(clazz).contains(col, `val`, INSENSITIVE).findAll()
    }
}