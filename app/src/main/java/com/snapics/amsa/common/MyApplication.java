package com.snapics.amsa.common;

import android.app.Application;

import com.facebook.stetho.Stetho;

import io.realm.Realm;

/**
 * Created by Mohamed Hamdan on 2017-Aug-01.
 * mohamed.nayef95@gmail.com
 */
public class MyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(this);
        Stetho.initializeWithDefaults(this);
    }
}
