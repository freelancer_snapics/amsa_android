package com.snapics.amsa.ui

import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.os.Handler
import android.os.Parcelable
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import butterknife.ButterKnife
import butterknife.OnClick
import com.google.gson.Gson
import com.loopj.android.http.RequestParams
import com.loopj.android.http.SyncHttpClient
import com.loopj.android.http.TextHttpResponseHandler
import com.snapics.amsa.Adapter.CategoriesAdapter
import com.snapics.amsa.Constants.ApiConstants
import com.snapics.amsa.Helper.httpHelper.HttpHelper
import com.snapics.amsa.Model.Categoriesmodel
import com.snapics.amsa.Model.FilteredModel
import com.snapics.amsa.Model.PharmaTakingDetail
import com.snapics.amsa.Model.RequestCategoryWrapper
import com.snapics.amsa.Model.SyncPharmaTakingDetail
import com.snapics.amsa.Model.SyncRequestCategoryWrapper
import com.snapics.amsa.Model.UserModel
import com.snapics.amsa.R
import com.snapics.amsa.data.DbHelper
import cz.msebera.android.httpclient.Header
import io.realm.RealmList
import kotlinx.android.synthetic.main.category_layout.etNote
import kotlinx.android.synthetic.main.category_layout.ryc_category
import kotlinx.android.synthetic.main.category_layout.tv_date
import kotlinx.android.synthetic.main.category_layout.tv_pharmacy_id
import kotlinx.android.synthetic.main.category_layout.tv_pharmacy_name
import kotlinx.android.synthetic.main.category_layout.tv_username
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.concurrent.thread

/*
 * Created by mac on 7/28/17.
 */
class CategoryActivity : AppCompatActivity(), Callback<ResponseBody> {

    private val handler = Handler()
    private var categoriesmodelList: MutableList<Categoriesmodel>? = null
    private var pairntList: MutableList<String>? = null
    private var filteredModelList: MutableList<FilteredModel>? = null
    private var id: String? = null
    private var wrapper: RequestCategoryWrapper? = null
    private var pharmaTakingDetail: MutableList<RequestCategoryWrapper>? = null
    private var isSync = false
    private var progress: ProgressDialog? = null
    private var adapter: CategoriesAdapter? = null
    private var isSyncTapped = false

    private var childIndex: Int = 0
    private var flatPosition: Int = 0

    private var startDate: String = ""
    private var endDate: String = ""

    companion object {
        private var CAMERA_REQUEST: Int = 100
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.category_layout)

        ButterKnife.bind(this)

        val calendar = Calendar.getInstance()
        val year = calendar.get(Calendar.YEAR)
        val month = calendar.get(Calendar.MONTH)
        val day = calendar.get(Calendar.DAY_OF_MONTH)
        val hour = calendar.get(Calendar.HOUR_OF_DAY)
        val minute = calendar.get(Calendar.MINUTE)
        startDate = year.toString() + "-" + month + "-" + day + " " + hour + ":" + minute

        progress = ProgressDialog(this)
        progress?.setTitle("Loading")
        progress?.setMessage("Wait Save Data...")
        progress?.setCancelable(false)

        tv_username.text = DbHelper.instance1.getFirst(UserModel::class.java)?.username
        tv_date.text = DbHelper.instance1.getFirst(UserModel::class.java)?.dateTimeNow

        filteredModelList = ArrayList()
        categoriesmodelList = intent.getParcelableArrayListExtra<Categoriesmodel>("categoriesList")
        val collec = categoriesmodelList?.map { it.itM_CAT }

        if (categoriesmodelList!!.size > 0) {
            val name = intent.extras.getString("name")
            if (name != null && name.isNotEmpty()) {
                tv_pharmacy_name?.text = name
            } else {
                tv_pharmacy_name?.text = (categoriesmodelList as MutableList)[0].pharmacY_NAME
            }
            id = intent.getStringExtra("Id")
            tv_pharmacy_id?.text = "pharmacy: $id"
        }


        pairntList = collec as MutableList
        val hs = HashSet<String>()
        hs.addAll(pairntList!!)
        pairntList?.clear()
        pairntList?.addAll(hs)

        Collections.sort(pairntList)

        for (title in pairntList!!) {
            val collec2 = categoriesmodelList?.filter { it.itM_CAT == title }
            if (!collec2!!.isEmpty()) {
                (collec2 as MutableList).removeAt(0)
            }
            filteredModelList?.add(FilteredModel(title, collec2))
        }

        val layoutManager = LinearLayoutManager(this)

        adapter = CategoriesAdapter(this, filteredModelList)
        ryc_category.isNestedScrollingEnabled = false
        ryc_category?.layoutManager = layoutManager
        ryc_category?.adapter = adapter

        adapter?.setOnClickListener { _, childIndex ->
            this.childIndex = childIndex
            this.flatPosition = flatPosition
            val cameraIntent = Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE)
            startActivityForResult(cameraIntent, CAMERA_REQUEST)
        }
    }

    @OnClick(R.id.btn_scan)
    fun scan() {
        val i = Intent(this, ScanActivity::class.java)
        startActivity(i)
        finish()
    }

    @OnClick(R.id.btn_save)
    fun save() {
        thread {
            isSync = false
            var send = false
            wrapper = RequestCategoryWrapper()
            val list = RealmList<PharmaTakingDetail>()
            filteredModelList?.forEach { item ->
                if (item.categoriesmodelList.any { it.gettAKINGQTY() == -1 }) {
                    send = false
                } else {
                    send = true
                    item.categoriesmodelList.forEach { item1 ->
                        val model = PharmaTakingDetail()
                        model.itmcat = item1.itM_CAT
                        model.adsbal = item1.adS_BAL.toString()
                        model.freezeqty = item1.freezE_QTY.toString()
                        model.itmname = item1.itM_NAME
                        model.itmpart = item1.itM_PART
                        model.notes = item1.getnOTES()

                        if (item1.imageFile != null) {
                            val client = SyncHttpClient()
                            val params = RequestParams()
                            params.put("file", item1.imageFile)
                            client.post("https://www.amsa-merchandiser.com/StockTakingService/api/upload", params, object : TextHttpResponseHandler() {
                                override fun onSuccess(statusCode: Int, headers: Array<out Header>?, responseString: String?) {
                                    model.ptidphoto = ApiConstants.IMAGES_PATH + item1.imageFile.name
                                }

                                override fun onFailure(statusCode: Int, headers: Array<out Header>?, responseString: String?, throwable: Throwable?) {
                                }

                            })
                        }
                        model.takingqty = item1.gettAKINGQTY().toString()
                        model.urgentflag = item1.getuRGENTFLAG()
                        list.add(model)
                    }
                    handler.post {
                        val calendar = Calendar.getInstance()
                        val year = calendar.get(Calendar.YEAR)
                        val month = calendar.get(Calendar.MONTH)
                        val day = calendar.get(Calendar.DAY_OF_MONTH)
                        val hour = calendar.get(Calendar.HOUR_OF_DAY)
                        val minute = calendar.get(Calendar.MINUTE)
                        endDate = year.toString() + "-" + month + "-" + day + " " + hour + ":" + minute
                        wrapper?.pharmaTakingDetails = list
                        wrapper?.notes = "${etNote?.text?.toString()}\n $startDate/$endDate"
                        wrapper?.user = DbHelper.instance1.getFirst(UserModel::class.java)?.firstname
                        wrapper?.pharmacyacc = id
                    }
                }
            }
            if (send) {
                handler.post {
                    progress?.show()
                }
                HttpHelper.getInstance().apiMethods.saveData(wrapper).enqueue(this)
            } else {
                handler.post {
                    Toast.makeText(this, "Please fill all data", Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    override fun onResponse(call: Call<ResponseBody>?, response: Response<ResponseBody>) {
        progress?.dismiss()
        if (response.isSuccessful) {
            val result = response.body()?.string()
            if (result?.equals("\"Ok\"", true) == true) {
                if (isSync) {
                    DbHelper.instance1.beginTransaction()
                    pharmaTakingDetail?.filter { !it.isDeleted }!![0].isDeleted = true
                    DbHelper.instance1.commitTransaction()
                    if (pharmaTakingDetail?.filter { !it.isDeleted }?.size == 0) {
                        isSyncTapped = false
                        Toast.makeText(this, "Sync success", Toast.LENGTH_LONG).show()
                        isSync = false
                    } else {
                        startSync()
                    }
                    return
                }
                Toast.makeText(this, "Saved online successfully", Toast.LENGTH_LONG).show()
            } else {
                isSyncTapped = false
                Toast.makeText(this, "Failed sync " + pharmaTakingDetail?.filter { !it.isDeleted }!![0].pharmacyacc, Toast.LENGTH_LONG).show()
                return
            }
        } else {
            if (isSync) {
                progress?.dismiss()
                Toast.makeText(this, "Sync failed", Toast.LENGTH_LONG).show()
                return
            }
            wrapper?.isDeleted = false
            DbHelper.instance1.insert(wrapper!!)
            Toast.makeText(this, "Saved offline successfully", Toast.LENGTH_LONG).show()
        }
        val i = Intent(this, CategoryActivity::class.java)
        i.putParcelableArrayListExtra("categoriesList", categoriesmodelList as ArrayList<out Parcelable>)
        i.putExtra("Id", id)
        startActivity(i)
        finish()
    }

    override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {
        progress?.dismiss()
        if (isSync) {
            Toast.makeText(this, "Sync failed", Toast.LENGTH_LONG).show()
        } else {
            wrapper?.isDeleted = false
            DbHelper.instance1.insert(wrapper!!)
            Toast.makeText(this, "Saved offline successfully", Toast.LENGTH_LONG).show()
            val i = Intent(this, CategoryActivity::class.java)
            i.putParcelableArrayListExtra("categoriesList", categoriesmodelList as ArrayList<out Parcelable>)
            i.putExtra("Id", id)
            startActivity(i)
            finish()
        }
    }

    override fun onBackPressed() {
        AlertDialog.Builder(this)
                .setMessage("Are you sure")
                .setPositiveButton("Ok") { _, _ ->
                    val intent = Intent(this, ScanActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    startActivity(intent)
                }
                .setNegativeButton("Cancel") { p0, _ ->
                    p0.cancel()
                }
                .show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
            val photo = data?.extras?.get("data") as Bitmap
            adapter?.onActivityResult(flatPosition, childIndex, ScanActivity.saveFile(photo, System.currentTimeMillis().toString()))
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.home_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.sync_tapped -> {
                if (isSyncTapped) {
                    return false
                }
                isSyncTapped = true
                startSync()
            }
            R.id.scan_tapped -> {
                val i = Intent(this, ScanActivity::class.java)
                startActivity(i)
                finish()
            }
            R.id.logout_tapped -> {
                val i = Intent(this, LogInActivity::class.java)
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(i)

                DbHelper.instance1.deleteAll(UserModel::class.java)
                DbHelper.instance1.deleteAll(PharmaTakingDetail::class.java)
                DbHelper.instance1.deleteAll(RequestCategoryWrapper::class.java)
            }
            R.id.preview_tapped -> {
                val i = Intent(this, PreviewActivity::class.java)
                startActivity(i)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun startSync() {
        pharmaTakingDetail = DbHelper.instance1.getAllByColVal(RequestCategoryWrapper::class.java, "isDeleted", false)
        if (pharmaTakingDetail != null && pharmaTakingDetail?.size != 0) {
            isSync = true

            if (progress?.isShowing == true) {
                progress?.dismiss()
            } else {
                progress?.show()
            }
            val wrapper = SyncRequestCategoryWrapper()
            wrapper.user = pharmaTakingDetail!![0].user
            wrapper.prepohoto = pharmaTakingDetail!![0].prepohoto
            wrapper.notes = pharmaTakingDetail!![0].notes
            wrapper.pharmacyacc = pharmaTakingDetail!![0].pharmacyacc
            wrapper.postpohoto = pharmaTakingDetail!![0].postpohoto
            wrapper.pharmaTakingDetails = mutableListOf()
            pharmaTakingDetail!![0].pharmaTakingDetails?.forEach {
                val details = SyncPharmaTakingDetail()
                details.takingqty = it.takingqty
                details.adsbal = it.adsbal
                details.freezeqty = it.freezeqty
                details.itmcat = it.itmcat
                details.itmname = it.itmname
                details.itmpart = it.itmpart
                details.notes = it.notes
                details.urgentflag = it.urgentflag
                details.ptidphoto = it.ptidphoto
                wrapper.pharmaTakingDetails?.add(details)
            }
            val json = Gson().toJson(wrapper)
            Log.e("json", json)
            HttpHelper.getInstance().apiMethods.saveData(wrapper).enqueue(this)
        } else {
            isSync = false
        }
    }
}
