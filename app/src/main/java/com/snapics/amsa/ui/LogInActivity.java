package com.snapics.amsa.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.widget.Toast;

import com.snapics.amsa.Helper.httpHelper.HttpHelper;
import com.snapics.amsa.Model.UserModel;
import com.snapics.amsa.R;
import com.snapics.amsa.data.DbHelper;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mac on 7/27/17.
 */

public class LogInActivity extends AppCompatActivity implements Callback<UserModel> {

    @BindView(R.id.edt_username)
    AppCompatEditText edtUserName;
    @BindView(R.id.edt_password)
    AppCompatEditText edtPassword;
    ProgressDialog progress;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_layout);
        ButterKnife.bind(this);

        if (DbHelper.Companion.getInstance1().getFirst(UserModel.class) != null) {
            Intent i = new Intent(LogInActivity.this, ScanActivity.class);
            startActivity(i);
            finish();
        }
    }


    @Override
    public void onResponse(Call<UserModel> call, Response<UserModel> response) {

        progress.dismiss();
        if (response.isSuccessful()) {
            UserModel userModel = response.body();
            if (userModel != null) {
                DbHelper.Companion.getInstance1().insert(userModel);
            }
            Intent i = new Intent(LogInActivity.this, ScanActivity.class);
            startActivity(i);
            finish();
        }
    }

    @Override
    public void onFailure(Call<UserModel> call, Throwable t) {
        progress.dismiss();
        if (t.getMessage().contains("JSON")) {
            Toast.makeText(LogInActivity.this, getResources().getString(R.string.incorrect_message), Toast.LENGTH_LONG).show();
            return;
        }
        Toast.makeText(LogInActivity.this, getResources().getString(R.string.no_internet), Toast.LENGTH_LONG).show();
    }

    @OnClick(R.id.btn_login)
    public void login() {
        Boolean isValid = true;
        if (edtUserName.getText().toString().isEmpty()) {

            isValid = false;
            edtUserName.setError("Please Enter username ");
        }

        if (edtPassword.getText().toString().isEmpty()) {

            isValid = false;
            edtPassword.setError("Please Enter password ");

        }

        if (!isValid) {

            return;
        }

        progress = new ProgressDialog(this);
        progress.setTitle("Loading");
        progress.setMessage("Wait while loading...");
        progress.setCancelable(false);
        progress.show();
        Call<UserModel> userCall = HttpHelper.getInstance().getApiMethods().login
                (edtUserName.getText().toString(), edtPassword.getText().toString());

        userCall.enqueue(this);
    }
}
