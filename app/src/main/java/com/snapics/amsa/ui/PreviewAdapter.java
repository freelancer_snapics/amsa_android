package com.snapics.amsa.ui;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.snapics.amsa.Model.RequestCategoryWrapper;
import com.snapics.amsa.R;

import java.util.List;

/**
 * Created by Mohamed Hamdan on 2017-Oct-11.
 * mohamed.nayef95@gmail.com
 */
public class PreviewAdapter extends RecyclerView.Adapter<PreviewAdapter.ViewHolder> {

    private Context context;
    private List<RequestCategoryWrapper> list;

    public PreviewAdapter(Context context, List<RequestCategoryWrapper> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_preview, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.tvRowPreview.setText(list.get(position).getPharmacyacc());
        ViewCompat.setBackgroundTintList(
                holder.tvRowPreview,
                list.get(position).isDeleted() ? ColorStateList.valueOf(Color.GREEN) : ColorStateList.valueOf(Color.GRAY)
        );
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        AppCompatTextView tvRowPreview;

        public ViewHolder(View itemView) {
            super(itemView);
            tvRowPreview = itemView.findViewById(R.id.tvRowPreview);
        }
    }
}
