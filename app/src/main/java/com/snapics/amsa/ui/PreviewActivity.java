package com.snapics.amsa.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.snapics.amsa.Model.RequestCategoryWrapper;
import com.snapics.amsa.R;
import com.snapics.amsa.data.DbHelper;

/**
 * Created by Mohamed Hamdan on 2017-Oct-11.
 * mohamed.nayef95@gmail.com
 */
public class PreviewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preivew);
        RecyclerView rvPreview = (RecyclerView) findViewById(R.id.rvPreview);
        rvPreview.setLayoutManager(new LinearLayoutManager(this));
        rvPreview.setAdapter(new PreviewAdapter(this,
                DbHelper.Companion.getInstance1().getAll(RequestCategoryWrapper.class)));
    }
}
