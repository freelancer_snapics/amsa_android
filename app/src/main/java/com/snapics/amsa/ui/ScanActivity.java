package com.snapics.amsa.ui;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.google.zxing.Result;
import com.kishan.askpermission.AskPermission;
import com.kishan.askpermission.ErrorCallback;
import com.kishan.askpermission.PermissionCallback;
import com.kishan.askpermission.PermissionInterface;
import com.snapics.amsa.Helper.httpHelper.HttpHelper;
import com.snapics.amsa.Model.Categoriesmodel;
import com.snapics.amsa.Model.Qrmodel;
import com.snapics.amsa.R;
import com.snapics.amsa.Utilities.Utilities;
import com.snapics.amsa.data.DbHelper;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.dm7.barcodescanner.zxing.ZXingScannerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/*
 * Created by mac on 7/28/17.
 */

public class ScanActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler,
        PermissionCallback, ErrorCallback, Callback<List<Categoriesmodel>> {


    private static final int REQUEST_PERMISSIONS = 20;
    public static Activity activity;

    ProgressDialog progress;
    @BindView(R.id.zx_scan)
    ZXingScannerView mScannerView;

    String Id = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.scan_layout);
        ButterKnife.bind(this);
        reqPermission();

        activity = this;
        if (Utilities.isAndroidEmulator()) {
            request("47010187");
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this);
        mScannerView.startCamera();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mScannerView.stopCamera();

    }

    @Override
    public void handleResult(Result result) {
        request(result.getText());
        mScannerView.resumeCameraPreview(this);
    }

    void request(String id) {
        Id = id;
        progress = new ProgressDialog(this);
        progress.setTitle("Loading");
        progress.setMessage("Wait while loading...");
        progress.setCancelable(false);
        if (Utilities.isNetworkAvailable(this)) {
            progress.show();
            Call<List<Categoriesmodel>> listCall = HttpHelper.getInstance().getApiMethods().getCategories(id);
            listCall.enqueue(this);
        } else {
            Qrmodel qrmodel = DbHelper.Companion.getInstance().getFirstByColVal(Qrmodel.class, "QR_ID", String.valueOf(Double.valueOf(id).doubleValue()));
            if (qrmodel != null) {
                List<Categoriesmodel> categoriesmodels = DbHelper.Companion.getInstance().getAllByColVal(Categoriesmodel.class, "PHARAMCY_ACC", String.valueOf(Double.valueOf(id).doubleValue()));
                if (categoriesmodels != null) {
                    ArrayList<Categoriesmodel> categories = new ArrayList<>();
                    for (Categoriesmodel model : categoriesmodels) {
                        categories.add(model);
                    }

                    Intent i = new Intent(ScanActivity.this, CategoryActivity.class);
                    i.putParcelableArrayListExtra("categoriesList", categories);
                    i.putExtra("name", qrmodel.getName());
                    i.putExtra("Id", Id);
                    i.putExtra("name", qrmodel.getName());
                    startActivity(i);
                } else {
                    Toast.makeText(ScanActivity.this, getResources().getString(R.string.incorrect_qr), Toast.LENGTH_LONG).show();
                }
            } else {
                Toast.makeText(ScanActivity.this, getResources().getString(R.string.incorrect_qr), Toast.LENGTH_LONG).show();
            }
        }
    }

    private void reqPermission() {
        new AskPermission.Builder(this).setPermissions(
                Manifest.permission.CAMERA,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
        ).setCallback(this)
                .setErrorCallback(this)
                .request(REQUEST_PERMISSIONS);
    }

    @Override
    public void onShowRationalDialog(final PermissionInterface permissionInterface, int requestCode) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.req_permissions);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                permissionInterface.onDialogShown();
            }
        });
        builder.setNegativeButton(R.string.cancel, null);
        builder.show();
    }

    @Override
    public void onShowSettings(final PermissionInterface permissionInterface, int requestCode) {


        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.need_permissions);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                permissionInterface.onSettingsShown();
            }
        });
        builder.setNegativeButton(R.string.cancel, null);
        builder.show();
    }

    @Override
    public void onPermissionsGranted(int requestCode) {
    }

    @Override
    public void onPermissionsDenied(int requestCode) {
        Toast.makeText(this, R.string.permissions_denied, Toast.LENGTH_LONG).show();

    }


    @Override
    public void onResponse(@NonNull Call<List<Categoriesmodel>> call, @NonNull Response<List<Categoriesmodel>> response) {
        progress.dismiss();
        if (response.isSuccessful()) {
            List<Categoriesmodel> categoriesmodelList = response.body();
            if (categoriesmodelList != null) {
                Log.d("TEST", categoriesmodelList.size() + "");
                DbHelper.Companion.getInstance().insert(categoriesmodelList);
                if (categoriesmodelList.size() > 0) {
                    Intent i = new Intent(ScanActivity.this, CategoryActivity.class);
                    i.putParcelableArrayListExtra("categoriesList", (ArrayList<? extends Parcelable>) categoriesmodelList);
                    i.putExtra("Id", Id);
                    startActivity(i);
                    return;
                }
            }
            Toast.makeText(ScanActivity.this, getResources().getString(R.string.incorrect_qr), Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void onFailure(Call<List<Categoriesmodel>> call, Throwable t) {
        if (t.getMessage().contains("JSON")) {
            Toast.makeText(ScanActivity.this, getResources().getString(R.string.incorrect_message), Toast.LENGTH_LONG).show();
            return;
        }
        Toast.makeText(ScanActivity.this, getResources().getString(R.string.no_internet), Toast.LENGTH_LONG).show();
    }

    public static File saveFile(Bitmap finalBitmap, String image_name) {
        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root);
        myDir.mkdirs();
        String fname = "Image-" + image_name + ".jpg";
        File file = new File(myDir, fname);
        if (file.exists()) file.delete();
        Log.i("LOAD", root + fname);
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return file;
    }
}